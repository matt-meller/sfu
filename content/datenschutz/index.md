---
title: "Datenschutz"
date: "2022-03-20"
layout: "single"
---

*Stand: 20.03.2022*

Mit diesen Datenschutzinformationen möchten wir Sie gemäß Art. 13 und 14 Datenschutz-Grundverordnung (DS-GVO) darüber informieren, wie wir Ihre personenbezogenen Daten im Zusammenhang mit dem Besuch auf unserer Website http://studentsforukraine.de/ verarbeiten. Bitte nehmen Sie sich einen Augenblick Zeit, um sich mit den Informationen vertraut zu machen.

#### 1. Name und Kontaktdaten des Verantwortlichen

Verantwortlicher für die Datenverarbeitung im Sinne von Art. 4 Nr. 7 DS-GVO ist:

Students for Ukraine\
E-Mail: info@studentsforukraine.de\
(nachfolgend auch „wir“ oder „uns“)

#### 2. Kategorien personenbezogener Daten

Wir verarbeiten die folgenden Kategorien personenbezogener Daten von Ihnen:

- Nutzungsdaten (z.B. IP-Adresse, Datum und Uhrzeit der Anfrage, Zeitzonendifferenz zur Greenwich Mean Time (GMT), Inhalt der Anforderung (konkrete Seite), Zugriffsstatus/HTTP-Statuscode, jeweils übertragene Datenmenge, Browser, Betriebssystem und dessen Oberfläche, Sprache und Version der Browsersoftware, Referrer URL)
- Kontaktinformationen (z.B. E-Mail, Name, Inhalt und Datum Ihrer Anfrage)

#### 3. Zwecke der Datenverarbeitung

Wir verarbeiten Ihre personenbezogenen Daten, soweit dies erforderlich ist, um die folgenden Zwecke zu erreichen:

- Bearbeitung Ihrer Kontaktanfrage
- Anzeige der Website und Gewährleistung ihrer Stabilität und Sicherheit

#### 4. Rechtsgrundlage der Datenverarbeitung

Wir verarbeiten Ihre personenbezogenen Daten nur im Einklang mit den europäischen und deutschen Datenschutzvorschriften, insbesondere der Datenschutz-Grundverordnung (DS-GVO) und dem Bundesdatenschutzgesetz (BDSG). Konkret bedeutet das, dass wir Ihre personenbezogenen Daten insbesondere verarbeiten, sofern, soweit und solange

- Sie uns eine Einwilligung erteilt haben (Art. 6 Abs. 1 a) DS-GVO),
- es für die Erfüllung eines Vertrags mit Ihnen oder zur Durchführung vorvertraglicher Maßnahmen, die auf Ihre Anfrage erfolgen, erforderlich ist (Art. 6 Abs. 1 b) DS-GVO),
- es für die Erfüllung gesetzlicher Verpflichtungen, denen wir unterliegen, erforderlich ist (Art. 6 Abs. 1 c) DS-GVO) oder
- es zur Wahrung berechtigter Interessen von uns oder von Dritten erforderlich ist (Art. 6 Abs. 1 f) DS-GVO).

Sofern wir in Ausnahmefällen sogenannte besondere Kategorien personenbezogener Daten (Daten, aus denen die  ethnische Herkunft, politische Meinungen, religiöse oder weltanschauliche Überzeugungen oder die Gewerkschaftszugehörigkeit hervorgehen, sowie die Verarbeitung von genetischen Daten, biometrischen Daten zur eindeutigen Identifizierung eines Menschen, Gesundheitsdaten oder Daten zum Sexualleben oder der sexuellen Orientierung) von Ihnen verarbeiten, erfolgt dies insbesondere, sofern, soweit und solange

- Sie uns eine ausdrückliche Einwilligung erteilt haben (Art. 9 Abs. 2 a) DS-GVO),
- es erforderlich ist, damit wir oder Sie die uns bzw. Ihnen aus dem Arbeitsrecht und dem Recht der sozialen Sicherheit und des Sozialschutzes erwachsenden Rechte ausüben und unseren bzw. Ihren diesbezüglichen Pflichten nachkommen kann (Art. 9 Abs. 2 b) DS-GVO),
- es zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen erforderlich ist (Art. 9 Abs. 2 b) DS-GVO)
- es sich die Verarbeitung auf personenbezogene Daten bezieht, die Sie offensichtlich öffentlich gemacht haben (Art. 9 Abs. 2 e) DS-GVO), oder
- es für Zwecke der Gesundheitsvorsorge oder der Arbeitsmedizin, für die Beurteilung der Arbeitsfähigkeit des Beschäftigten, für die medizinische Diagnostik, die Versorgung oder Behandlung im Gesundheits- oder Sozialbereich oder für die Verwaltung von Systemen und Diensten im Gesundheits- oder Sozialbereich erforderlich ist (Art. 9 Abs. 2 h) DS-GVO).

#### 5. Empfänger oder Kategorien von Empfängern der personenbezogenen Daten

Wir geben Ihre personenbezogenen Daten nur an andere Empfänger weiter, wenn dies zur Erfüllung der beschriebenen Zwecke erforderlich ist, Sie uns Ihre Einwilligung hierfür erteilt haben oder wir gesetzlich oder aufgrund einer gerichtlichen oder behördlichen Anordnung dazu verpflichtet sind.

- Empfänger im Rahmen des regulären Besuchs der Website, insb.:
    - IT-Dienstleister
    - Marketing-Dienstleister
    - Druck-Dienstleister
    - Beratung und Consulting
    - Inkasso
    - Akten- und Datenträgerentsorgung
    - Staatliche Stellen (z.B. Finanzbehörden) 

- Empfänger (Sonderfälle), insb.:
    - Staatliche Stellen (z.B. Gerichte und Staatsanwaltschaft)    
    - Potenzielle und tatsächliche Erwerber des Unternehmens

#### 6. Übermittlung von personenbezogenen Daten an Drittstaaten

Grundsätzlich werden Ihre personenbezogenen Daten in Deutschland und im europäischen Ausland verarbeitet. Findet ausnahmsweise eine Verarbeitung Ihrer personenbezogenen Daten in Ländern außerhalb der Europäischen Union oder des Europäischen Wirtschaftsraums (sog. Drittstaaten) statt, geschieht dies nur, soweit durch bestimmte Schutzmaßnahmen sichergestellt ist, dass hierfür ein angemessenes Datenschutzniveau besteht. Typischerweise ergreifen wir dazu folgende Schutzmaßnahmen:

- Angemessenheitsbeschluss der EU-Kommission: Empfänger in Andorra, Argentinien, Kanada, Färöer Inseln, Guernsey, Israel, Isle of Man, Japan, Jersey, Neuseeland, Schweiz, Uruguay (Weitere Informationen unter https://ec.europa.eu/info/law/law-topic/data-protection/international-dimension-data-protection/adequacy-decisions_en)
- Standardvertragsklauseln: Sonstige Empfänger (Weitere Informationen unter https://ec.europa.eu/info/law/law-topic/data-protection/international-dimension-data-protection/standard-contractual-clauses-scc_en)
- Ausnahmen nach Art. 49 DS-GVO: Sonstige Empfänger.
 
Weitere Informationen über Drittstaattransfers oder Kopien dieser Maßnahmen können Sie unter den genannten Kontaktadressen erhalten.

#### 7. Speicherdauer
Wir bewahren Ihre personenbezogenen Daten nur so lange auf, wie es notwendig ist, um die beschriebenen Zwecke erfüllen zu können. Die Prüfung der hierfür erforderlichen Zeiträume erfolgt durch eine sorgfältige Abwägung, im Rahmen derer wir die Erforderlichkeit der Datenverarbeitung genau prüfen: 

- Verarbeiten wir Ihre personenbezogenen Daten auf Grundlage Ihrer Einwilligung, so geschieht dies längstens bis Sie Ihre Einwilligung widerrufen. 
- Verarbeiten wir Ihre personenbezogenen Daten auf Grundlage unserer berechtigten Interessen, so geschieht dies längstens bis zu Ihrem berechtigten Widerspruch.
- Im Übrigen bewahren wir Ihre personenbezogene Daten nur auf, soweit dies zur Erfüllung unserer vertraglichen und gesetzlichen Pflichten oder zur Erhaltung von Beweismitteln im Rahmen der gesetzlichen Verjährungsvorschriften erforderlich ist. Die wichtigsten gesetzlichen Aufbewahrungspflichten ergeben sich aus § 257 Handelsgesetzbuch (HGB) und § 147 Abgabenordnung (AO) und betragen sechs bzw. zehn Jahre. Die gesetzlichen Verjährungsfristen können gemäß §§ 195 ff. des Bürgerlichen Gesetzbuches (BGB) bis zu dreißig Jahre betragen, wobei die regelmäßige Verjährungsfrist drei Jahre beträgt.
 
Nach Ablauf der geltenden Aufbewahrungsfristen werden wir Ihre personenbezogenen Daten auf sichere Weise löschen oder anonymisieren.

#### 8. Freiwilligkeit oder Verpflichtung zur Bereitstellung der Daten

Im Rahmen des Besuchs der Website müssen Sie uns nur diejenigen personenbezogenen Daten bereitstellen, die für die Nutzung der Website  erforderlich sind oder zu deren Erhebung wir gesetzlich verpflichtet sind. Wenn Sie uns diese Daten nicht bereitstellen, werden Sie regelmäßig nicht in der Lage, unsere Website zu besuchen.

#### 9. Herkunft Ihrer personenbezogenen Daten
Maßgeblich verarbeiten wir personenbezogene Daten, die wir bei Besuch der Website von Ihnen direkt erhalten.
Teilweise verarbeiten wir auch personenbezogene Daten, die wir nicht direkt von Ihnen erhalten haben. Das betrifft solche Daten, die wir zulässigerweise aus öffentlich zugänglichen Quellen (z.B. Internet, Presse, Handels- und Vereinsregister) gewinnen oder die uns von anderen Gruppenunternehmen oder von sonstigen Dritten (z.B. einer Kreditauskunftei) berechtigt übermittelt werden.

#### 10. Betroffenenrechte
Unter der Datenschutz-Grundverordnung stehen Ihnen verschiedene Betroffenenrechte zu. Bitte haben Sie dafür Verständnis, dass einzelne Rechte in bestimmten Fällen eingeschränkt sein können. Sollte dies der Fall sein, werden wir Ihnen den Grund hierfür mitteilen:

- Recht auf Widerruf (Art. 7 Abs. 3 DS-GVO)
- Recht auf Auskunft (Art. 15 DS-GVO)
- Recht auf Berichtigung (Art. 16 DS-GVO)
- Recht auf Löschung (Art. 17 DS-GVO)
- Recht auf Einschränkung der Datenverarbeitung (Art. 18 DS-GVO)
- Recht auf Datenübertragbarkeit (Art. 20 DS-GVO)
- Recht auf Widerspruch (Art. 21 DS-GVO)
 
Für die Ausübung dieser Rechte (Weitere Informationen unter https://ec.europa.eu/info/law/law-topic/data-protection/reform/rights-citizens/my-rights_de) wenden Sie sich bitte an die genannten Kontaktadressen. Wenn Sie der Auffassung sein sollten, dass wir bei der Verarbeitung Ihrer personenbezogenen Daten datenschutzrechtliche Vorschriften nicht beachtet haben, können Sie sich nach Art. 77 DS-GVO mit einer Beschwerde an die zuständige Aufsichtsbehörde wenden.

#### 11. Bestehen automatisierter Entscheidungsfindung
Wir nutzen keine automatisierten Entscheidungsfindungen im Sinne von Art. 22 DS-GVO.
